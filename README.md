# iCalendar generator
[![SensioLabsInsight](https://insight.sensiolabs.com/projects/739b9b66-3fe3-4f5e-93a7-d7fe7d874707/big.png)](https://insight.sensiolabs.com/projects/739b9b66-3fe3-4f5e-93a7-d7fe7d874707)

This simple class generate an *.ics file. require php >= 8.2.1

## Usage

```php
<?php

require_once 'vendor/autoload.php';

use Ical\Ical;
use Ical\IcalendarException;
try {
        $ical = (new Ical())->setAddress('Paris')
                ->setDateStart(new \DateTime('2014-11-21 15:00:00'))
                ->setDateEnd(new \DateTime('2014-11-21 16:00:00'))
                ->setDescription('wonder description')
                ->setSummary('Running')
                ->setOrganizer('foo@bar.fr') //optional
                ->setFilename(uniqid());
                ->setStatus('CONFIRMED') //optional
                ->setSequence(2) //Number of updates, the default is at 1, optional
            $ical->addHeader();

    echo $ical->getICAL();

        } catch (IcalendarException $exc) {
            echo $exc->getMessage();
}


```
