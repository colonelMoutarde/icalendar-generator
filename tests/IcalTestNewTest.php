<?php

declare(strict_types=1);

use Ical\Exceptions\IcalendarException;
use Ical\Ical;
use PHPUnit\Framework\TestCase;

class IcalTestNewTest extends TestCase
{
    public function testNameIsSetCorrectly(): void
    {
        $ical = new Ical();
        $ical->setName('Test Calendar');
        self::assertSame('Test Calendar', $ical->getName());
    }

    public function testTimezoneICalIsSetCorrectly(): void
    {
        $ical = new Ical();
        $ical->setTimezoneICal('America/New_York');
        self::assertSame('America/New_York', $ical->getTimezoneICal());
    }

    public function testDateStartIsSetCorrectly(): void
    {
        $ical = new Ical();
        $date = new \DateTime('2023-10-10 10:00:00');
        $ical->setDateStart($date);
        self::assertSame('20231010T100000Z', $ical->getDateStart());
    }

    public function testSummaryIsSetCorrectly(): void
    {
        $ical = new Ical();
        $ical->setSummary('Meeting');
        self::assertSame('Meeting', $ical->getSummary());
    }

    public function testDateEndIsSetCorrectly(): void
    {
        $ical = new Ical();
        $date = new \DateTime('2023-10-10 12:00:00');
        $ical->setDateEnd($date);
        self::assertSame('20231010T120000Z', $ical->getDateEnd());
    }

    public function testFilenameIsSetCorrectly(): void
    {
        $ical = new Ical();
        $ical->setFilename('calendar');
        self::assertSame('calendar', $ical->getFilename());
    }

    public function testAddressIsSetCorrectly(): void
    {
        $ical = new Ical();
        $ical->setAddress('123 Main St');
        self::assertSame('123 Main St', $ical->getAddress());
    }

    public function testDescriptionIsSetCorrectly(): void
    {
        $ical = new Ical();
        $ical->setDescription('Description');
        self::assertSame('Description', $ical->getDescription());
    }

    public function testAlarmIsSetCorrectly(): void
    {
        $ical = new Ical();
        $ical->setAlarm(true);
        self::assertTrue($ical->getAlarm());
    }

    public function testRepeatIsSetCorrectly(): void
    {
        $ical = new Ical();
        $ical->setRepeat(true);
        self::assertTrue($ical->getRepeat());
    }

    /**
     * @throws IcalendarException
     */
    public function testOrganizerIsSetCorrectly(): void
    {
        $ical = new Ical();
        $ical->setOrganizer('test@example.com');
        self::assertSame('test@example.com', $ical->getOrganizer());
    }

    public function testInvalidOrganizerThrowsException(): void
    {
        $this->expectException(IcalendarException::class);
        $ical = new Ical();
        $ical->setOrganizer('invalid-email');
    }

    public function testSequenceIsSetCorrectly(): void
    {
        $ical = new Ical();
        $ical->setSequence(2);
        self::assertSame(2, $ical->getSequence());
    }

    /**
     * @throws IcalendarException
     */
    public function testStatusIsSetCorrectly(): void
    {
        $ical = new Ical();
        $ical->setStatus('CONFIRMED');
        self::assertSame('CONFIRMED', $ical->getStatus());
    }

    public function testInvalidStatusThrowsException(): void
    {
        $this->expectException(IcalendarException::class);
        $ical = new Ical();
        $ical->setStatus('INVALID_STATUS');
    }

    /**
     * @throws IcalendarException
     */
    public function testIcalGenerationIsCorrect(): void
    {
        $ical = new Ical();
        $ical->setName('Test Calendar')
            ->setTimezoneICal('Europe/Paris')
            ->setDateStart(new \DateTime('2023-10-10 10:00:00'))
            ->setDateEnd(new \DateTime('2023-10-10 12:00:00'))
            ->setSummary('Meeting')
            ->setFilename('calendar')
            ->setAddress('123 Main St')
            ->setDescription('Description')
            ->setAlarm(true)
            ->setRepeat(true)
            ->setOrganizer('test@example.com')
            ->setSequence(1)
            ->setStatus('CONFIRMED');

        $icalString = $ical->getICAL();
        self::assertStringContainsString('BEGIN:VCALENDAR', $icalString);
        self::assertStringContainsString('VERSION:2.0', $icalString);
        self::assertStringContainsString('PRODID:Test Calendar', $icalString);
        self::assertStringContainsString('DTSTART:20231010T100000Z', $icalString);
        self::assertStringContainsString('DTEND:20231010T120000Z', $icalString);
        self::assertStringContainsString('SUMMARY:Meeting', $icalString);
        self::assertStringContainsString('ORGANIZER:MAILTO:test@example.com', $icalString);
        self::assertStringContainsString('LOCATION:123 Main St', $icalString);
        self::assertStringContainsString('DESCRIPTION:Description', $icalString);
        self::assertStringContainsString('SEQUENCE:1', $icalString);
        self::assertStringContainsString('STATUS:CONFIRMED', $icalString);
        self::assertStringContainsString('END:VCALENDAR', $icalString);
    }

}
