<?php

declare(strict_types=1);

/**
 * Copyright (C) 2021 luc Sanchez.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

use Ical\Exceptions\IcalendarException;
use Ical\Ical;
use PHPUnit\Framework\TestCase;

class IcalTest extends TestCase
{
    private Ical $ical;

    private string $sampleIcal;

    /**
     * @return void
     * @throws IcalendarException
     */
    protected function setUp(): void
    {
        $this->ical = (new Ical())
            ->setName('test')
            ->setAddress('Paris')
            ->setDateStart(new \DateTimeImmutable('2014-11-21 15:00:00'))
            ->setDateEnd(new \DateTimeImmutable('2014-11-21 16:00:00'))
            ->setDescription('wonder description')
            ->setSummary('Running')
            ->setOrganizer('foo@bar.fr') //optional
            ->setFilename('myFileName')
            ->setStatus('CONFIRMED') //optional
            ->setSequence(2);

        $this->sampleIcal = $this->getTestIcalStub();

    }

    public function testSetGetAddress(): void
    {
        $this->ical->setAddress('my Address');
        self::assertSame('my Address', $this->ical->getAddress());
    }

    public function testSetGetTimezoneICal(): void
    {
        $this->ical->setTimezoneICal('Europe/Berlin');
        self::assertSame('Europe/Berlin', $this->ical->getTimezoneICal());
    }

    public function testSetGetAlarm(): void
    {
        $this->ical->setAlarm(true);
        self::assertTrue($this->ical->getAlarm());
    }

    public function testSetGetRepeat(): void
    {
        $this->ical->setRepeat(true);
        self::assertTrue($this->ical->getRepeat());
    }

    public function testSetGetSummary(): void
    {
        $this->ical->setSummary('Summary');
        self::assertSame('Summary', $this->ical->getSummary());
    }

    public function testSetGetFilename(): void
    {
        $this->ical->setFilename('Filename');
        self::assertSame('Filename', $this->ical->getFilename());
    }

    public function testSetGetSequence(): void
    {
        $this->ical->setSequence(1);
        self::assertSame(1, $this->ical->getSequence());
    }

    public function testSetGetDescription(): void
    {
        $this->ical->setDescription('Description');
        self::assertSame('Description', $this->ical->getDescription());
    }

    public function testGetICAL(): void
    {
        self::assertSame($this->sampleIcal, $this->ical->getICAL('myUid'));
    }

    public function testGetICALNotEqual(): void
    {
        self::assertNotSame($this->sampleIcal, $this->ical->getICAL());
    }

    public function testSetGetName(): void
    {
        $this->ical->setName('Name');
        self::assertSame('Name', $this->ical->getName());
    }

    public function testSetGetDateEnd(): void
    {
        self::assertSame('20141121T160000Z', $this->ical->getDateEnd());
    }

    /**
     * @return void
     * @throws IcalendarException
     */
    public function testSetGetOrganizerOK(): void
    {
        $this->ical->setOrganizer('testemail@email.fr');
        self::assertSame('testemail@email.fr', $this->ical->getOrganizer());
    }

    /**
     * @return void
     * @throws IcalendarException
     */
    public function testSetOrganizerKO(): void
    {
        $this->expectException(IcalendarException::class);
        $this->ical->setOrganizer('testEmailmalformed');
    }

    public function testSetGetDateStart(): void
    {
        self::assertSame('20141121T150000Z', $this->ical->getDateStart());
    }

    private function getTestIcalStub(): string
    {
        $ical = file_get_contents(__DIR__ . '/stubs/icalStub.ical');

        if (false === $ical) {
            throw new \RuntimeException('Could not open icalStub.');
        }

        return $ical;
    }
}
