<?php

declare(strict_types=1);

/**
 * Copyright (C) 2014 luc Sanchez.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

namespace Ical;

use Ical\Enum\StatusEnum;
use Ical\Exceptions\IcalendarException;

class Ical
{
    private string $name;

    private string $timezoneICal = 'Europe/Paris';

    private string $dateStart;

    private string $summary;

    private string $dateEnd;

    private string $filename;

    private string $address;

    private string $description;

    private bool $alarm = false;

    private bool $repeat = false;

    private bool|string $organizer = false;

    private int $sequence = 1;

    private string $status;

    public function getName(): string
    {
        return $this->name;
    }

    public function getTimezoneICal(): string
    {
        return $this->timezoneICal;
    }

    public function getDateStart(): string
    {
        return $this->dateStart;
    }

    public function getSummary(): string
    {
        return $this->summary;
    }

    public function getDateEnd(): string
    {
        return $this->dateEnd;
    }

    public function getFilename(): string
    {
        return $this->filename;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getAlarm(): bool
    {
        return $this->alarm;
    }

    public function getRepeat(): bool
    {
        return $this->repeat;
    }

    public function getOrganizer(): bool|string
    {
        return $this->organizer;
    }

    public function getSequence(): int
    {
        return $this->sequence;
    }

    public function getStatus(): string
    {
        return $this->status;
    }
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function setTimezoneICal(string $timezoneICal): self
    {
        $this->timezoneICal = $timezoneICal;

        return $this;
    }

    public function setDateStart(\DateTimeInterface $dateStart): self
    {
        $this->dateStart = $this->dateToCal($dateStart);

        return $this;
    }

    public function setSummary(string $summary): self
    {
        $this->summary = $summary;

        return $this;
    }

    public function setDateEnd(\DateTimeInterface $dateEnd): self
    {
        $this->dateEnd = $this->dateToCal($dateEnd);

        return $this;
    }

    public function setFilename(string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function setAlarm(bool $alarm): self
    {
        $this->alarm = $alarm;

        return $this;
    }

    public function setRepeat(bool $repeat): self
    {
        $this->repeat = $repeat;

        return $this;
    }

    /**
     * @throws IcalendarException
     */
    public function setOrganizer(string $organizer): static
    {
        if (false === filter_var($organizer, FILTER_VALIDATE_EMAIL)) {
            throw new IcalendarException(__METHOD__.' --> Email is empty or invalid !', 500);
        }

        $this->organizer = $organizer;

        return $this;
    }

    /**
     * Number of updates, the first update is at 1.
     */
    public function setSequence(int $review): self
    {
        $this->sequence = $review;

        return $this;
    }

    /**
     * @throws IcalendarException
     */
    public function setStatus(string $status): self
    {
        $this->status = StatusEnum::fromName($status);

        return $this;
    }

    public function getICAL(?string $uid = null): string
    {
        $iCal = 'BEGIN:VCALENDAR'."\n";
        $iCal .= 'VERSION:2.0'."\n";
        $iCal .= 'PRODID:'.$this->getName()."\n";
        $iCal .= 'CALSCALE:GREGORIAN'."\n";
        $iCal .= 'BEGIN:VEVENT'."\n";
        $iCal .= 'DTSTART:'.$this->getDateStart()."\n";
        $iCal .= 'DTEND:'.$this->getDateEnd()."\n";
        $iCal .= 'SUMMARY:'.$this->escapeString($this->getSummary())."\n";

        if (null === $uid) {
            $uid = uniqid('', true);
        }

        $iCal .= 'UID:'.$uid."\n";

        if (false !== $this->getOrganizer()) {
            $iCal .= 'ORGANIZER:MAILTO:'.$this->getOrganizer()."\n";
        }

        $iCal .= 'LOCATION:'.$this->escapeString($this->getAddress())."\n";
        $iCal .= 'DESCRIPTION:'.$this->escapeString($this->getDescription())."\n";
        $iCal .= 'SEQUENCE:'.$this->getSequence()."\n";

        if ($this->getAlarm()) {
            $iCal .= 'BEGIN:VALARM'."\n";
            $iCal .= 'ACTION:DISPLAY'."\n";
            $iCal .= 'DESCRIPTION:Reminder'."\n";
            $iCal .= 'TRIGGER:-PT'.$this->getAlarm().'M'."\n";
            if ($this->getRepeat()) {
                $iCal .= 'REPEAT:'.$this->getRepeat()."\n";
            }
            $iCal .= 'END:VALARM'."\n";
        }

        if ($this->getStatus()) {
            $iCal .= 'STATUS:'.$this->getStatus()."\n";
        }

        $iCal .= 'END:VEVENT'."\n";
        $iCal .= 'END:VCALENDAR'."\n";

        return $iCal;
    }

    public function addHeader(): void
    {
        header('Content-type: text/calendar; charset=utf-8');
        header('Content-Disposition: attachment; filename='.$this->getFilename().'.ics');
    }

    private function dateToCal(\DateTimeInterface $timestamp): string
    {
        return $timestamp->format('Ymd\THis\Z');
    }

    private function escapeString(string $string): ?string
    {
        return preg_replace('/([,;])/', '\\\$1', $string);
    }
}
