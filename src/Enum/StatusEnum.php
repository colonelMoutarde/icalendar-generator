<?php

declare(strict_types=1);

namespace Ical\Enum;

use Ical\Exceptions\IcalendarException;

enum StatusEnum: string
{
    case TENTATIVE = 'TENTATIVE';
    case CONFIRMED = 'CONFIRMED';
    case CANCELLED = 'CANCELLED';

    /**
     * @throws IcalendarException
     */
    public static function fromName(string $name): string
    {
        foreach (self::cases() as $status) {
            if ($name === $status->name) {
                return $status->value;
            }
        }
        throw new IcalendarException("$name is not a valid backing value for enum ".self::class);
    }
}
