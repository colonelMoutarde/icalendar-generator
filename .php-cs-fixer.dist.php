<?php

declare(strict_types=1);

use PhpCsFixer\Runner\Parallel\ParallelConfigFactory;
use PhpCsFixer\Finder;
use PhpCsFixer\Config;

$finder = (new Finder())
    ->in(__DIR__)
    ->exclude([
        'vendor',
        ]);

return (new Config())
    ->setRules([
        '@PhpCsFixer:risky' => true,
        'modernize_strpos' => true,
        '@PSR12' => true,
        'phpdoc_summary' => false,
        'strict_param' => true,
        'yoda_style' => true,
        'declare_strict_types' => true,
        'array_syntax' => ['syntax' => 'short'],
    ])
    ->setFinder($finder)
    ->setParallelConfig(ParallelConfigFactory::detect())
    ;
